var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        app: './src/main.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.bundle.js'
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    module:{
        rules:[
            {
                test:/\.css$/,
                use:['style-loader','css-loader']
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,  
                use: [{
                    loader: 'url-loader',
                    options: { 
                        limit: 80000, // Convert images < 8mb to base64 strings
                        name: 'images/[hash]-[name].[ext]'
                    } 
                }]
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
       ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            //'process.env.NODE_ENV': JSON.stringify('production')
            'process.env.NODE_ENV': JSON.stringify('dev')
    }) ],
    devServer: {
        contentBase: path.join(__dirname, "/"),
        compress: true,
        inline: true,
        port: 8888,
        open: true,
        publicPath: "/dist/",
        hot: true,
        watchOptions: {
                poll: true
            }
        },
    stats: { colors: true }
};