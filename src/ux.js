document.body.onload = () => {
    document.body.onscroll = function() {
        let y = window.scrollY;
        
        if(y < 20){
            var contenedor = document.querySelector(".container");
            contenedor.style.transform = "rotate(-30deg) skew(25deg) scale(0.8)";
            contenedor.style.transformOrigin = "center";
            var vpx = document.querySelector("#veintepx");
            vpx.style.opacity = "0";
            vpx.style.visibility = "hidden";
        }

        if(y >= 20){
            var contenedor = document.querySelector(".container");
            contenedor.style.transform = "rotate(0deg) skew(0deg) scale(0.8)";
            contenedor.style.transform = "translate(-250px,0px)";
            contenedor.style.transformOrigin = "center";

            var vpx = document.querySelector("#veintepx");
            vpx.style.opacity = "1";
            vpx.style.position = "fixed";
            vpx.style.visibility = "visible";

            var cvpx = document.querySelector("#cientoveintepx");
            cvpx.style.opacity = "0";
            cvpx.style.position = "fixed";
            cvpx.style.visibility = "hidden";

            var element = document.querySelector(".container img:nth-child(3)");
            element.style.transform = "rotateY(0deg)";
            element.style.transformOrigin = "left";
        }

        if(y >= 120){
            var element = document.querySelector(".container img:nth-child(3)");
            element.style.transform = "rotateY(-90deg)";
            element.style.transformOrigin = "left";

            var vpx = document.querySelector("#veintepx");
            vpx.style.visibility = "hidden";
            vpx.style.opacity = "0";
            var cvpx = document.querySelector("#cientoveintepx");
            cvpx.style.opacity = "1";
            cvpx.style.position = "fixed";
            cvpx.style.visibility = "visible";
        }

    }
}